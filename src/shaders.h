#pragma once
#include <glad/glad.h>
#include "buffers.h"
typedef struct _shader {
  buffer super;
  const char *source;
} shader;

typedef struct _shader_program {
  buffer super;
} shader_program;

extern struct _vtable shader_table;
extern struct _vtable shader_program_table;
extern void _shader_bind(shader *s);
extern void _shader_program_bind(shader *s);
extern void shader_init(shader *s, GLenum t, const char *const path,
                        const char *const filename);
extern void shader_delete(shader *s);
extern void shader_compile(shader *s);
extern void shader_program_init(shader_program *s);
extern void shader_program_attach(shader_program *prog, shader *shad);
extern void shader_program_link(shader_program *prog);
extern void shader_program_use(shader_program *prog);
extern int shader_readf(const char **writeto, const char *const filename);