#ifndef BUFFERS_H
#define BUFFERS_H
#include <glad/glad.h>

typedef struct _buffer {
  GLuint id;
  struct _vtable *vtable;
} buffer;

typedef struct _vbo {
  buffer super;
} vbo;

typedef struct _vao {
  buffer super;
} vao;

struct _vtable {
  void (*bind)(buffer *b);
};

struct _vtable vbo_table;
struct _vtable vao_table;

void vbo_bind(vbo *o);
void vao_bind(vao *o);

void buffer_init(buffer *b);

void buffer_bind(buffer *b);
extern void vbo_init(vbo *o);
extern void vao_init(vao *o);
#endif
