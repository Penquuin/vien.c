#include "buffers.h"

struct _vtable vbo_table = {&vbo_bind};
struct _vtable vao_table = {&vao_bind};

void vbo_bind(vbo *o) { glBindBuffer(GL_ARRAY_BUFFER, o->super.id); }
void vao_bind(vao *o) { glBindVertexArray(o->super.id); }

void buffer_init(buffer *b) { b->id = 0; }

void buffer_bind(buffer *b) { b->vtable->bind(b); }

void vbo_init(vbo *o) {
  buffer_init(&(o->super));
  o->super.vtable = &vbo_table;
  glGenBuffers(1, &(o->super.id));
}
void vao_init(vao *o) {
  buffer_init(&(o->super));
  o->super.vtable = &vao_table;
  glGenVertexArrays(1, &(o->super.id));
}
