#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <stdio.h>

#include "buffers.h"
#include "shaders.h"

void framebuffer_size_callback(GLFWwindow *window, int width, int height);
void processInput(GLFWwindow *window);

float vertices[] = {-0.5f, -0.5f, 0.0f, 0.5f, -0.5f, 0.0f, 0.0f, 0.5f, 0.0f};

int main(int argc, char **argv) {
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  GLFWwindow *window = glfwCreateWindow(800, 600, "VIEN beta", 0, 0);
  if (window == NULL) {
    printf("Failed to create GLFW window");
    glfwTerminate();
    return -1;
  }
  glfwMakeContextCurrent(window);

  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
    printf("Failed to initialize GLAD");
    return -1;
  }
  glViewport(0, 0, 900, 800);
  glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

  shader vertexShader;
  shader_init(&vertexShader, GL_VERTEX_SHADER, __SHADERDIR__, "vertex.glsl");
  shader_compile(&vertexShader);

  shader fragmentShader;
  shader_init(&fragmentShader, GL_FRAGMENT_SHADER, __SHADERDIR__,
              "fragment.glsl");
  shader_compile(&fragmentShader);

  shader_program shaderProgram;
  shader_program_init(&shaderProgram);
  shader_program_attach(&shaderProgram, &vertexShader);
  shader_program_attach(&shaderProgram, &fragmentShader);
  shader_program_link(&shaderProgram);

  shader_delete(&vertexShader);
  shader_delete(&fragmentShader);

  vbo ArrayBuffer;
  vbo_init(&ArrayBuffer);
  buffer_bind((buffer *const)&ArrayBuffer);

  vao VertexArray;
  vao_init(&VertexArray);
  buffer_bind((buffer *const)&VertexArray);

  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *)0);
  glEnableVertexAttribArray(0);

  if (argc <= 1) {
    while (!glfwWindowShouldClose(window)) {
      processInput(window);
      glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
      glClear(GL_COLOR_BUFFER_BIT);
      shader_program_use(&shaderProgram);
      buffer_bind((buffer *const)&ArrayBuffer);
      glDrawArrays(GL_TRIANGLES, 0, 3);
      glfwPollEvents();
      glfwSwapBuffers(window);
    }
  } else {
    printf("<CD/CI> Successfully created Vien...\n");
  }

  glfwTerminate();
  return 0;
}

void processInput(GLFWwindow *window) {
  if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    glfwSetWindowShouldClose(window, 1);
}

void framebuffer_size_callback(GLFWwindow *window, int width, int height) {
  glViewport(0, 0, width, height);
}
