/*
  * !TODO: Shaders must be implemented after
  * instantiation.
  */
#include "shaders.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct _vtable shader_program_table = {&_shader_program_bind};
struct _vtable shader_table = {&_shader_bind};

void _shader_bind(shader *s) { printf("_shader_bind is not implemented"); }

void _shader_program_bind(shader *s) {
  printf("_shader_program_bind is not implemented");
}

void shader_init(shader *s, GLenum t, const char *const path,
                 const char *const filename) {
  s->super.vtable = &shader_table;
  s->super.id = glCreateShader(t);
  int pl = strlen(path), fl = strlen(filename);
  char *cpath = (char *)calloc(fl + pl + 1, sizeof(char));
  strcpy_s(cpath, pl + 1, path);
  strcat_s(cpath, fl + pl + 1, filename);

  shader_readf(&s->source, cpath);
  free(cpath);//free the memory before it leaks itself :)
  cpath = NULL;
}
void shader_delete(shader *s) {
  glDeleteShader(s->super.id);
  free(s->source);
  s->source = NULL;
}

void shader_compile(shader *s) {
  glShaderSource(s->super.id, 1, &s->source, NULL);
  glCompileShader(s->super.id);
}

void shader_program_init(shader_program *s) {
  s->super.vtable = &shader_program_table;
  s->super.id = glCreateProgram();
}

void shader_program_attach(shader_program *prog, shader *shad) {
  glAttachShader(prog->super.id, shad->super.id);
}

void shader_program_link(shader_program *prog) {
  glLinkProgram(prog->super.id);
}

void shader_program_use(shader_program *prog) { glUseProgram(prog->super.id); }

int shader_readf(const char **writeto, const char *const filename) {
  FILE *h;
  fopen_s(&h, filename, "r");
  if (h == NULL)
    return 0;
  const char *content;
  int file_size;
  fseek(h, 0L, SEEK_END);
  file_size = ftell(h);
  rewind(h);
  content = (char *)malloc((file_size + 1) * sizeof(char));
  fread(content, 1, file_size, h);
  fclose(h);
  // the pointer content will be freed while writeto will go on
  *writeto = content;
  // TODO: Add free(h); and test the result as soon as i went back
  // home.
  return file_size;
};
