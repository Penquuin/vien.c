# vien.c

[![pipeline status](https://gitlab.com/Penquuin/vien.c/badges/main/pipeline.svg)](https://gitlab.com/Penquuin/vien.c/-/commits/main)

Vien.c, an OpenGL-based rendering system, is currently developed by and maintained by Penquuin. Dispatches of pull requests may be issued, but they will not be definitely handled or accepted by him.

## Prospects

